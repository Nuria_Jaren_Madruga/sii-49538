#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"

int main() { 

	int i; 
	DatosMemCompartida* dat; 
	char* org;

	int file=open("/tmp/datosComp.txt",O_RDWR);

	org=(char*)mmap(NULL,sizeof(*(dat)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);

	dat=(DatosMemCompartida*)org;
	float posicion;

 while(1) { 

	posicion = (dat->raqueta1.y2+dat->raqueta1.y1)/2;
	if(posicion<dat->esfera.centro.y)
		dat->accion=1;
	else if (posicion>dat->esfera.centro.y)
		dat->accion=-1;
	else 
		dat->accion=0;

 	usleep(25000); 

 }

	munmap(org,sizeof(*(dat)));
	return 0;

}
